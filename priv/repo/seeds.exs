# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Treillis.Repo.insert!(%Treillis.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Treillis.Repo
alias Treillis.Applicants.ApplicantStatus
alias Treillis.Applicants.Applicant

%{id: applicant_status_id} =
  Repo.insert!(%ApplicantStatus{name: "not_met", label_fr: "à rencontrer"})

Repo.insert!(%ApplicantStatus{name: "interview", label_fr: "entretien"})

Repo.insert!(%Applicant{
  email: "milou@tintin.be",
  fullname: "Milou le Chien",
  avatar_url:
    "https://i.pinimg.com/originals/00/a9/2c/00a92c86690156b2d6874b89755812b5.jpg",
  position: "Toutou fidèle",
  score: 4.2,
  likes: 4,
  comments: 2,
  messages: 6,
  applicant_status_id: applicant_status_id
})

Repo.insert!(%Applicant{
  email: "eusebe@chevaliers.fr",
  fullname: "Eusèbe",
  avatar_url:
    "https://i.pinimg.com/originals/9b/8f/d6/9b8fd685b76b9356d87def0d1b4394b2.jpg",
  position: "Apprenti chevalier",
  score: 5.0,
  likes: 8,
  comments: 12,
  messages: 9,
  applicant_status_id: applicant_status_id
})

Repo.insert!(%Applicant{
  email: "fourreux@akbar.fr",
  fullname: "Fourreux",
  avatar_url:
    "https://i.pinimg.com/originals/5e/e0/de/5ee0deaa13d03fccd05e5a71674e68bc.jpg",
  position: "Étrange animal",
  score: 3.2,
  likes: 2,
  comments: 4,
  messages: 3,
  applicant_status_id: applicant_status_id
})
