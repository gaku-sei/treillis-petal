defmodule Treillis.Repo.Migrations.CreateApplicantStatuses do
  use Ecto.Migration

  def change do
    create table(:applicant_statuses) do
      add :name, :string, null: false
      add :label_fr, :string, null: false
      add :order, :serial, null: false

      timestamps()
    end

    create unique_index(:applicant_statuses, :name)
    create unique_index(:applicant_statuses, :label_fr)
  end
end
