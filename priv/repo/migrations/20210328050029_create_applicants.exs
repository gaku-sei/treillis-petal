defmodule Treillis.Repo.Migrations.CreateApplicants do
  use Ecto.Migration

  def change do
    create table(:applicants) do
      add :email, :string, null: false
      add :fullname, :string, null: false
      add :avatar_url, :string
      add :position, :string, null: false
      add :score, :float
      add :likes, :integer, default: 0
      add :comments, :integer, default: 0
      add :messages, :integer, default: 0
      add :applicant_status_id, references(:applicant_statuses), null: false

      timestamps()
    end

    create index(:applicants, :applicant_status_id)
    create unique_index(:applicants, :email)
  end
end
