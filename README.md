# Treillis

Le Trello en Treillis, construit avec la stack [PETAL](https://changelog.com/posts/petal-the-end-to-end-web-stack).

## Bien Commencer

### Avec [asdf](https://asdf-vm.com/#/)

Si asdf est installé sur votre machine, vous pouvez installer les plugins suivants:

```
asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang.git
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git
asdf plugin-add nodejs https://github.com/asdf-vm/asdf-nodejs.git
```

Puis lancer la commande `asdf install`. Vous êtes prêt!

### Installation Manuelle

Vous devez installer [Node](https://nodejs.org/en/) et [Elixir](https://elixir-lang.org/install.html) (qui à son tour nécessite l'installation d'Erlang).

## Lancer le Serveur

- Installation des dépendences avec `mix deps.get`
- Création de la base de donnée (lance également les migrations et des données) `mix ecto.setup`
- Aller dans le dossier `assets` et lancer `npm install`
- Revenez au dossier racine pour lancer le serveur grâce à la commande `mix phx.server`

[`localhost:4000`](http://localhost:4000) devrait être lancé et fonctionel.

## Générer de la documentation

Vous pouvez lancer `mix docs` puis servir le dossier `doc` pour avoir accès à la documentation.

```bash
mix docs
npm install -g serve
serve doc
```

Vous pouvez visiter [htttp://localhost:5000](http://localhost:5000).

## Quelques Commandes Importantes

Au cours du développement de l'application vous pouvez utiliser les commandes suivantes afin de vous assurer que votre code est toujours propre et fonctionel:

- `mix compile` pour compiler
- `mix dialyzer` lance [Dialyzer](https://github.com/jeremyjh/dialyxir) qui va analyser notre code (notamment les types)
- `mix credo` lance [Credo](https://github.com/rrrene/credo/) un "linter" pour Elixir
- `mix format --check-formatted` valide le format du code (vous pouvez lancer `mix format` pour formatter le code si besoin)
- `mix sobelow` [Sobelow](https://github.com/nccgroup/sobelow) analyze les risquent encourus par contre application

## Sémantique

### Applicant

Un Applicant est, comme son nom l'indique, un individu qui a entamé une procédure de recrutement auprès d'une entreprise.

### Applicant Status

Un Applicant, une fois inscrit, est assigné à un "statut", ou "étape". Par défault "À rencontrer". Il est possible d'assigned un nouveau status à tout moment grâce à Treillis!

## Pour Quelques Données de Plus

En plus des données déjà existantes (trouvables dans le fichier `priv/repo/seeds.exs`), vous pouvez créer des Applicant Statuses:

```
create_applicant_status --name "had_first_interview" --label "premier entretien passé"
```

Ou encore des Applicants:

```
mix create_applicant --email "something@unique.com"
```

## Motivations

### Stack Technique

J'ai choisi la stack PETAL pour 3 raisons majeures:

- Proche de la stack utilisée à Welcome to the Jungle
- Correspond parfaitement à la tâche demandée, les websockets rendent les intéractions front/back et la synchronisation entre les clients triviales
- Apprendre [Alpine JS](https://github.com/alpinejs/alpine) et Live View (et continuer d'apprendre Elixir et Phoenix)

### Ce que j'ai aimé (voire adoré)

- Il est vraiment _très_ facile de synchroniser plusieurs navigateurs avec [`Broadcast`](https://hexdocs.pm/phoenix/Phoenix.Socket.Broadcast.html)
- Les templates Phoenix facilitent énormément la construction des pages
- La logique interne des composants Live View simple et qui semble couvrir de nombreux cas d'utilisation (la fonction `preload` est un bon exemple)
- Tous les plus d'Elixir et Phoenix (tests, tout-en-un, migrations, doc, etc...)
- Pas de Rest, pas de GraphQL, juste des websockets qui sont elles-mêmes transparentes
- Alpine est facile à prendre en main, très facile à comprendre mais...

### Ce que j'ai moins aimé

- ... Alpine est aussi difficile à maintenir, le code peut très rapidement devenir illisible s'il n'est pas dès le départ bien architecturé
- La logique du front... côté back ne rend pas l'application plus facile à écrire
- Même remarque en ce qui concerne Live View que pour Alpine, le code peut rapidement devenir peu lisible sans règles claires
- Conclusion, pour moi la stack demande un peu de maturation et de définir un ensemble de pratiques plus précises ([Surface](https://github.com/surface-ui/surface) par example pourrait aider)
- Bien que le compileur et les librairies donnent beaucoup d'indication en ce qui concerne les erreurs potentielles que l'application pourrait rencontrer, le manque de type safety et de liant entre les blocs donne parfois l'impression de jongler avec du mercure
- J'aime toujours le développement proprement frontend, [Solid](https://github.com/ryansolid/solid) notamment et les dernières itérations de l'API React, qui utilisé avec [ReScript](https://rescript-lang.org/) est réellement plaisant à utiliser pour moi
