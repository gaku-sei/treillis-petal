defmodule TreillisWeb.ApplicantsTest do
  use Treillis.DataCase

  import Treillis.Factory

  alias Treillis.Applicants
  alias Treillis.Applicants.Applicant
  alias Treillis.Applicants.ApplicantStatus

  describe "Treillis.Applicants.create_applicant/1" do
    test "creates an applicant" do
      %{
        email: email,
        applicant_status_id: applicant_status_id
      } = applicant = params_with_assocs(:applicant)

      assert {:ok,
              %Applicant{
                email: ^email,
                applicant_status_id: ^applicant_status_id
              }} = Applicants.create_applicant(applicant)
    end

    test "doesn't create an applicant with an invalid email" do
      applicant = params_with_assocs(:applicant, email: "whatever.com")

      assert {:error, changeset} = Applicants.create_applicant(applicant)

      {_, errors} = changeset.errors[:email]

      assert errors[:validation] == :format
    end
  end

  describe "Treillis.Applicants.update_applicant_status/2" do
    test "updates an applicant's applicant status" do
      %Applicant{
        id: applicant_id,
        applicant_status_id: original_applicant_status_id
      } = insert(:applicant)

      %ApplicantStatus{id: new_applicant_status_id} = insert(:applicant_status)

      assert original_applicant_status_id != new_applicant_status_id

      Applicants.update_applicant_status(applicant_id, new_applicant_status_id)

      assert %Applicant{
               id: ^applicant_id,
               applicant_status_id: ^new_applicant_status_id
             } = Applicants.get_applicant(applicant_id)
    end
  end

  describe "Treillis.Applicants.create_applicant_status/2" do
    test "creates an applicant status" do
      name = applicant_status_name()

      label_fr = applicant_status_label_fr()

      assert {:ok, %ApplicantStatus{name: ^name, label_fr: ^label_fr}} =
               Applicants.create_applicant_status(name, label_fr)
    end

    test "properly increments counter field" do
      {:ok, %ApplicantStatus{order: order1}} =
        Applicants.create_applicant_status(
          applicant_status_name(),
          applicant_status_label_fr()
        )

      {:ok, %ApplicantStatus{order: order2}} =
        Applicants.create_applicant_status(
          applicant_status_name(),
          applicant_status_label_fr()
        )

      assert order2 - 1 == order1
    end

    test "doesn't create more than one applicant status with the same name" do
      name = applicant_status_name()

      insert(:applicant_status, name: name)

      assert {:error, changeset} =
               Applicants.create_applicant_status(
                 name,
                 applicant_status_label_fr()
               )

      {_, errors} = changeset.errors[:name]

      assert errors[:constraint] == :unique
    end

    test "doesn't create more than one applicant status with the same French label" do
      label_fr = applicant_status_label_fr()

      insert(:applicant_status, label_fr: label_fr)

      assert {:error, changeset} =
               Applicants.create_applicant_status(
                 applicant_status_name(),
                 label_fr
               )

      {_, errors} = changeset.errors[:label_fr]

      assert errors[:constraint] == :unique
    end
  end

  describe "Treillis.Applicants.update_applicant_status_label_fr/2" do
    test "updates an applicant status' French label" do
      %ApplicantStatus{id: applicant_status_id, label_fr: original_label_fr} =
        insert(:applicant_status)

      new_label_fr = applicant_status_label_fr()

      assert original_label_fr != new_label_fr

      assert {:ok, %ApplicantStatus{label_fr: ^new_label_fr}} =
               Applicants.update_applicant_status_label_fr(
                 applicant_status_id,
                 new_label_fr
               )
    end
  end

  describe "Treillis.Applicants.get_applicant_statuses/0" do
    test "returns a list of created applicant statuses" do
      [%{name: name1}, %{name: name2}, %{name: name3}] =
        insert_list(3, :applicant_status)

      assert [
               %ApplicantStatus{name: ^name1},
               %ApplicantStatus{name: ^name2},
               %ApplicantStatus{name: ^name3}
             ] = Applicants.get_applicant_statuses()
    end

    test "returns a list of created applicant properly ordered" do
      insert_list(3, :applicant_status)

      [
        %ApplicantStatus{order: order1},
        %ApplicantStatus{order: order2},
        %ApplicantStatus{order: order3}
      ] = Applicants.get_applicant_statuses()

      assert order1 < order2
      assert order2 < order3
    end

    test "returns a list of created applicant statuses with their applicants attached" do
      [applicant_status1, applicant_status2] = insert_pair(:applicant_status)

      [%{id: applicant_id1}, %{id: applicant_id2}] =
        insert_pair(:applicant, applicant_status: applicant_status1)

      %{id: applicant_id3} =
        insert(:applicant, applicant_status: applicant_status2)

      assert [
               %ApplicantStatus{
                 applicants: [
                   %Applicant{id: ^applicant_id1},
                   %Applicant{id: ^applicant_id2}
                 ]
               },
               %ApplicantStatus{applicants: [%Applicant{id: ^applicant_id3}]}
             ] = Applicants.get_applicant_statuses()
    end
  end
end
