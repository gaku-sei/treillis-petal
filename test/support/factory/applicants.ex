defmodule Treillis.Factory.Applicants do
  @moduledoc """
  The applicants factories.
  """

  alias Treillis.Applicants.Applicant
  alias Treillis.Applicants.ApplicantStatus

  defmacro __using__(_opts) do
    quote do
      def applicant_factory do
        %Applicant{
          email: Faker.Internet.email(),
          fullname: Faker.Person.name(),
          avatar_url: Faker.Avatar.image_url(),
          position: Faker.Person.title(),
          score: Faker.random_between(0, 5) / 1,
          likes: Faker.random_between(0, 50),
          comments: Faker.random_between(0, 50),
          messages: Faker.random_between(0, 50),
          applicant_status: build(:applicant_status)
        }
      end

      def applicant_status_name do
        Faker.Lorem.sentence(2)
      end

      def applicant_status_label_fr do
        Faker.Lorem.sentence(3)
      end

      def applicant_status_factory do
        %ApplicantStatus{
          name: applicant_status_name(),
          label_fr: applicant_status_label_fr()
        }
      end
    end
  end
end
