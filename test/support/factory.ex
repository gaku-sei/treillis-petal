defmodule Treillis.Factory do
  @moduledoc """
  The main Factory module.

  Used [ex_machina](https://github.com/thoughtbot/ex_machina) under the hood.
  """

  use ExMachina.Ecto, repo: Treillis.Repo

  use Treillis.Factory.Applicants
end
