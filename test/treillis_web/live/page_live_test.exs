defmodule TreillisWeb.PageLiveTest do
  use TreillisWeb.ConnCase

  import Phoenix.LiveViewTest

  test "disconnected and connected render", %{conn: conn} do
    assert {:ok, _page_live, _disconnected_html} = live(conn, "/")
  end
end
