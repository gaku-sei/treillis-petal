const SyncLists = {
  mounted() {
    this.listener = ({ detail: { applicantStatusId, elementId } }) => {
      // Clean up the element id
      const applicantId = elementId.replace(/^applicant-/, "");

      this.pushEvent("dropped", { applicantId, applicantStatusId });
    };

    window.addEventListener("applicant-status-dropped", this.listener);
  },
  destroyed() {
    window.removeEventListener("applicant-status-dropped", this.listener);
  },
};

export const hooks = { SyncLists };
