defmodule Mix.Tasks.CreateApplicant do
  @moduledoc """
  Creates a random applicant from a unique email.
  The applicant will be attached to the applicant status which order is the lowest.

  ## Usage

    > mix create_applicant --email something@unique.com
  """

  use Mix.Task

  alias Treillis.Applicants
  alias Treillis.Applicants.ApplicantStatus

  @impl true
  def run(args) do
    {parsed_args, _argv} =
      OptionParser.parse!(args,
        aliases: [e: :email],
        strict: [email: :string]
      )

    if email = parsed_args[:email] do
      # Starts the app
      Mix.Task.run("app.start")

      %ApplicantStatus{id: applicant_status_id} =
        Enum.min_by(Applicants.get_applicant_statuses(), & &1.order)

      Applicants.create_applicant(%{
        email: email,
        fullname: Faker.Person.name(),
        avatar_url: Faker.Avatar.image_url(),
        position: Faker.Person.title(),
        score: Faker.random_between(0, 5) / 1,
        likes: Faker.random_between(0, 50),
        comments: Faker.random_between(0, 50),
        messages: Faker.random_between(0, 50),
        applicant_status_id: applicant_status_id
      })
    else
      # credo:disable-for-next-line
      IO.inspect(
        "You need to pass the email argument: mix create_applicant_status --email \"something@unique.com\""
      )
    end
  end
end
