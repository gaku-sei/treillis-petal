defmodule Mix.Tasks.CreateApplicantStatus do
  @moduledoc """
  Creates an applicant status from a name and a French label.
  Both must be unique.

  ## Usage

    > mix create_applicant_status --name had_first_interview --label "premier entretien passé"
  """

  use Mix.Task

  alias Treillis.Applicants

  @impl true
  def run(args) do
    {parsed_args, _argv} =
      OptionParser.parse!(args,
        aliases: [l: :label, n: :name],
        strict: [label: :string, name: :string]
      )

    label = parsed_args[:label]

    name = parsed_args[:name]

    if is_nil(label) or is_nil(name) do
      # credo:disable-for-next-line
      IO.inspect(
        "You need to pass 2 arguments, name and French label: create_applicant_status --name had_first_interview --label \"premier entretien passé\""
      )
    else
      # Starts the app
      Mix.Task.run("app.start")

      Applicants.create_applicant_status(name, label)
    end
  end
end
