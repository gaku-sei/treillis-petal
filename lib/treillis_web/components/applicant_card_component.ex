defmodule TreillisWeb.ApplicantCardComponent do
  @moduledoc """
  Defines the Applicant Card live component.
  It will display all the required information for the applicant (avatar, name, etc...)
  """
  use TreillisWeb, :live_component
end
