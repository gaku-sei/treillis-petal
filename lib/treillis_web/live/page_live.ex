defmodule TreillisWeb.PageLive do
  @moduledoc """
  Light.
  """

  use TreillisWeb, :live_view
  use Phoenix.HTML

  alias Phoenix.Socket.Broadcast
  alias Treillis.Applicants
  alias Treillis.Applicants.ApplicantStatus

  import TreillisWeb.Gettext

  require Logger

  @topic "light"

  @impl true
  def handle_event(
        "dropped",
        %{
          "applicantId" => applicant_id,
          "applicantStatusId" => applicant_status_id
        },
        socket
      ) do
    new_applicant_statuses =
      case Applicants.update_applicant_status(applicant_id, applicant_status_id) do
        {:ok, _} ->
          Applicants.get_applicant_statuses()

        {:error, error} ->
          Logger.error(error)
          socket.assigns.applicant_statuses
      end

    TreillisWeb.Endpoint.broadcast_from(self(), @topic, "dropped", %{
      applicant_statuses: new_applicant_statuses
    })

    {:noreply, assign(socket, :applicant_statuses, new_applicant_statuses)}
  end

  def handle_event(
        "switch_label_status",
        %{"applicant-status-id" => applicant_status_id},
        socket
      ) do
    new_socket =
      update(
        socket,
        :label_modes,
        &switch_applicant_status_mode(&1, applicant_status_id)
      )

    {:noreply, new_socket}
  end

  def handle_event(
        "save_new_applicant_status_label_fr",
        %{"applicant_status" => %{"label_fr" => ""}},
        socket
      ) do
    {:noreply,
     put_flash(socket, :error, gettext("The label should not be empty"))}
  end

  def handle_event(
        "save_new_applicant_status_label_fr",
        %{
          "applicant_status" => %{
            "id" => applicant_status_id,
            "label_fr" => label_fr
          }
        },
        socket
      ) do
    case Applicants.update_applicant_status_label_fr(
           applicant_status_id,
           label_fr
         ) do
      {:ok, _applicant} ->
        new_applicant_statuses =
          set_applicant_label_fr(
            socket.assigns.applicant_statuses,
            applicant_status_id,
            label_fr
          )

        new_label_modes =
          Map.put(
            socket.assigns.label_modes,
            applicant_status_id,
            :show
          )

        TreillisWeb.Endpoint.broadcast_from(self(), @topic, "updated", %{
          applicant_statuses: new_applicant_statuses
        })

        new_socket =
          assign(
            socket,
            applicant_statuses: new_applicant_statuses,
            label_modes: new_label_modes
          )

        {:noreply, new_socket}

      {:error, _} ->
        {:noreply,
         put_flash(
           socket,
           :error,
           gettext("An error occured while updating the applicant status label")
         )}
    end
  end

  def handle_event(_event, _params, socket) do
    {:noreply, socket}
  end

  # The following handlers have the exact same implementation for now.
  # In the future we should handle the 2 events independently.
  @impl true
  def handle_info(
        %Broadcast{topic: @topic, event: "dropped", payload: state},
        socket
      ) do
    {:noreply, assign(socket, state)}
  end

  def handle_info(
        %Broadcast{topic: @topic, event: "updated", payload: state},
        socket
      ) do
    {:noreply, assign(socket, state)}
  end

  @impl true
  def mount(_params, _session, socket) do
    TreillisWeb.Endpoint.subscribe(@topic)

    applicant_statuses = Applicants.get_applicant_statuses()

    {:ok,
     assign(socket,
       applicant_statuses: applicant_statuses,
       label_modes:
         for(
           applicant_status <- applicant_statuses,
           do: {applicant_status.id, :show},
           into: %{}
         )
     )}
  end

  # Takes a list of ApplicantStatus, an ApplicantStatus id, and the new label
  # and return the list updated
  @spec set_applicant_label_fr(
          applicant_statuses :: list(ApplicantStatus.t()),
          applicant_status_id :: String.t(),
          new_label_fr :: String.t()
        ) :: list(ApplicantStatus.t())
  defp set_applicant_label_fr(
         applicant_statuses,
         applicant_status_id,
         new_label_fr
       ) do
    Enum.map(
      applicant_statuses,
      fn %{id: id} = applicant_status ->
        if id == applicant_status_id,
          do: %{applicant_status | label_fr: new_label_fr},
          else: applicant_status
      end
    )
  end

  # Takes a map of ApplicantStatus id => mode, where mode is :edit or :show,
  # an ApplicantStatus id, and updates the modes map accordingly by switching
  # the proper value using `switch_label_mode`
  @spec switch_applicant_status_mode(
          modes :: map(),
          applicant_status_id :: String.t()
        ) :: map()
  defp switch_applicant_status_mode(modes, applicant_status_id) do
    Enum.reduce(modes, %{}, fn {id, mode}, acc ->
      mode =
        if applicant_status_id == id,
          do: switch_label_mode(mode),
          else: mode

      Map.put(acc, id, mode)
    end)
  end

  # Switch the mode from show to edit and from edit to show
  @spec switch_label_mode(:edit | :show) :: :edit | :show
  defp switch_label_mode(:edit), do: :show
  defp switch_label_mode(:show), do: :edit
end
