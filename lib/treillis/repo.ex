defmodule Treillis.Repo do
  use Ecto.Repo,
    otp_app: :treillis,
    adapter: Ecto.Adapters.Postgres
end
