defmodule Treillis.Applicants do
  @moduledoc """
  This module allows you to manage applicants and related resources (like the status).
  """

  alias Treillis.Repo
  alias Treillis.Applicants.Applicant
  alias Treillis.Applicants.ApplicantStatus

  import Ecto.Query
  import Ecto.Changeset

  @doc """
  Creates an applicant from a provided map.

  The applicant status is required and expected to be provided.
  """
  @spec create_applicant(params :: map()) ::
          {:ok, Applicant.t()} | {:error, Ecto.Changeset.t()}
  def create_applicant(params) do
    params
    |> Applicant.changeset_create()
    |> Repo.insert()
  end

  @doc """
  Gets an applicant from an id.
  """
  @spec get_applicant(id :: String.t()) :: Applicant.t() | nil
  def get_applicant(id) do
    Repo.get_by(Applicant, id: id)
  end

  @doc """
  Updates an applicant's applicant status.

  The applicant status must exist.
  """
  @spec update_applicant_status(
          applicant_id :: String.t(),
          applicant_status_id :: String.t()
        ) ::
          {:ok, Applicant.t()} | {:error, Ecto.Changeset.t()}
  def update_applicant_status(applicant_id, applicant_status_id) do
    with applicant <- Repo.get_by(Applicant, id: applicant_id),
         changed_applicant <-
           change(applicant, applicant_status_id: applicant_status_id) do
      Repo.update(changed_applicant)
    end
  end

  @doc """
  Creates an applicant status from a name and a French label. Both must be unique.
  """
  @spec create_applicant_status(name :: String.t(), label_fr :: String.t()) ::
          {:ok, ApplicantStatus.t()} | {:error, Ecto.Changeset.t()}
  def create_applicant_status(name, label_fr) do
    %{name: name, label_fr: label_fr}
    |> ApplicantStatus.changeset_create()
    |> Repo.insert(returning: true)
  end

  @doc """
  Updates an applicant status French label.
  Label must be unique.
  """
  @spec update_applicant_status_label_fr(
          applicant_status_id :: String.t(),
          label_fr :: String.t()
        ) ::
          {:ok, ApplicantStatus.t()} | {:error, Ecto.Changeset.t()}
  def update_applicant_status_label_fr(applicant_status_id, label_fr) do
    with applicant_status <-
           Repo.get_by(ApplicantStatus, id: applicant_status_id),
         changed_applicant_status <-
           change(applicant_status, label_fr: label_fr) do
      Repo.update(changed_applicant_status)
    end
  end

  @doc """
  Get all the an applicant statuses with their applicants attached.
  """
  @spec get_applicant_statuses() :: list(ApplicantStatus.t())
  def get_applicant_statuses() do
    ApplicantStatus
    |> preload(:applicants)
    |> order_by(asc: :order)
    |> Repo.all()
  end
end
