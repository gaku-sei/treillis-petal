defmodule Treillis.Schema do
  @moduledoc """
  The default Schema module to use everytime you'll need to define an Ecto Schema.

  It will automatically includes all the function neeeded for schema and changeset definition,
  and it'll set properly some fields settings.
  """

  defmacro __using__(_opts) do
    quote do
      use Ecto.Schema

      import Ecto.Changeset

      @primary_key {:id, :binary_id, autogenerate: true}

      @foreign_key_type :binary_id
    end
  end
end
