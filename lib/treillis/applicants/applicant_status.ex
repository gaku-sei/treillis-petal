defmodule Treillis.Applicants.ApplicantStatus do
  @moduledoc """
  Defines the applicant status schema.

  The status can include "applicant in review" or "applicant hired" for instance.
  """

  use Treillis.Schema

  alias Treillis.Applicants.Applicant

  @typedoc """
  The status itself.
  """
  @type t :: %__MODULE__{
          id: String.t(),
          name: String.t(),
          label_fr: String.t(),
          order: integer()
        }

  schema "applicant_statuses" do
    field :name, :string
    field :label_fr, :string
    field :order, :integer
    has_many :applicants, Applicant

    timestamps()
  end

  @doc false
  def changeset_create(schema \\ %__MODULE__{}, params) do
    schema
    |> cast(params, [:name, :label_fr])
    |> validate_required([:name, :label_fr])
    |> unique_constraint(:name)
    |> unique_constraint(:label_fr)
  end

  @doc false
  def changeset_update(schema \\ %__MODULE__{}, params) do
    schema
    |> cast(params, [:name, :label_fr])
    |> validate_required([:name, :label_fr])
    |> unique_constraint(:name)
    |> unique_constraint(:label_fr)
  end
end
