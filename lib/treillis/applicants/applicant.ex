defmodule Treillis.Applicants.Applicant do
  @moduledoc """
  The main applicant entity.
  """
  use Treillis.Schema

  alias Treillis.Applicants.ApplicantStatus

  @doc """
  Simple yet effective regex to check email is valid or not.
  Taken from [email_checker](https://github.com/jshmrtn/email_checker/blob/161fb679552c6123beb7637e8d78a0993b82dc75/lib/email_checker/tools.ex#L4).
  """
  @email_regex ~r/^(?<user>[^\s]+)@(?<domain>[^\s]+\.[^\s]+)$/

  @typedoc """
  The applicant.
  """
  @type t :: %__MODULE__{
          id: String.t(),
          email: String.t(),
          fullname: String.t(),
          avatar_url: String.t() | nil,
          position: String.t(),
          score: float() | nil,
          likes: integer(),
          comments: integer(),
          messages: integer(),
          applicant_status: ApplicantStatus.t()
        }

  schema "applicants" do
    field :email, :string
    field :fullname, :string
    field :avatar_url, :string
    field :position, :string
    field :score, :float
    field :likes, :integer
    field :comments, :integer
    field :messages, :integer
    belongs_to :applicant_status, ApplicantStatus

    timestamps()
  end

  @doc false
  def changeset_create(schema \\ %__MODULE__{}, params) do
    schema
    |> cast(params, [
      :email,
      :fullname,
      :avatar_url,
      :position,
      :score,
      :likes,
      :comments,
      :messages,
      :applicant_status_id
    ])
    |> validate_required([
      :email,
      :fullname,
      :position,
      :likes,
      :comments,
      :messages,
      :applicant_status_id
    ])
    |> validate_format(:email, @email_regex)
    |> unique_constraint(:email)
  end
end
