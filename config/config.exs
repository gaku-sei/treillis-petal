# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :treillis,
  ecto_repos: [Treillis.Repo]

# Configures the endpoint
config :treillis, TreillisWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base:
    "Iug+ruTmYtrQDpExjIrpMGDtY2sJZz8hAaj0Kq43Sb2cusvd7rfB4WHuqtHcCncS",
  render_errors: [
    view: TreillisWeb.ErrorView,
    accepts: ~w(html json),
    layout: false
  ],
  pubsub_server: Treillis.PubSub,
  live_view: [signing_salt: "Y6FikpCR"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

config :gettext, :default_locale, "fr"
